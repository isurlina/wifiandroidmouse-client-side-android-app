package com.example.androidwifimouse;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;


public class options extends Activity {
	
	public static final int DEFAULT = 0;
	public static final int DEFAULT_GAMEPAD = 0;
	public static final int DEFAULT_POWER = 0;
	public static final int DEFAULT_MODE = 0;
	
	 /* if click_touch is 0 then click-touch is OFF */
	 public static int click_touch;
	 /*if remember_pos is 1 then in gamepad mode stick remember last position*/
	 public static int remember_pos = 1;
	
	int radiocheck = 1;
	
	protected void onCreate(Bundle savedInstanceState) {
		
		SharedPreferences sharedPreferences = getSharedPreferences ("MyOptions" , Context.MODE_PRIVATE);
    	int motion_click = sharedPreferences.getInt("motion_click", DEFAULT_GAMEPAD);
		int power_safe = sharedPreferences.getInt("power_safe", DEFAULT_POWER);
		int gamepad_mode = sharedPreferences.getInt("gamepad_mode", DEFAULT_MODE);
		
    	
		
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.options);
		
	 	Display display = getWindowManager().getDefaultDisplay(); 
	 	
	
	 	
	 	final ToggleButton toggleTouchClick = (ToggleButton) findViewById(R.id.ToggleTouchClick);
		if (motion_click == 1)
 		{
			toggleTouchClick.setChecked(true);
 			MotionActivity.motion_click = 1;
 		}
	 	else
	 	{
	 		toggleTouchClick.setChecked(false);
	 		MotionActivity.motion_click = 0;
	 		
	 	}
		
	 	toggleTouchClick.setOnClickListener(new OnClickListener()
	 	{
	 		@Override
	 		public void onClick(View v) 
	 		{
	 			if(toggleTouchClick.isChecked())
	 			{
	 				Toast.makeText(getApplicationContext(), "Click-touch is on", Toast.LENGTH_SHORT).show();
	 				SharedPreferences sharedPreferences = getSharedPreferences("MyOptions", Context.MODE_PRIVATE);
	 				SharedPreferences .Editor editor = sharedPreferences.edit();
	 				editor.putInt("motion_click", 1);
	 				editor.commit();
	 				MotionActivity.motion_click = 1;
	 			}
	 			else 
	 			{
	 				Toast.makeText(getApplicationContext(), "Click-touch is off", Toast.LENGTH_SHORT).show();
	 				SharedPreferences sharedPreferences = getSharedPreferences("MyOptions", Context.MODE_PRIVATE);
	 				SharedPreferences .Editor editor = sharedPreferences.edit();
	 				editor.putInt("motion_click", 0);
	 				editor.commit();
	 				MotionActivity.motion_click = 0;
	 			}
	 		}
		});
	 	
	 	
	 	
	 	
	 	
	 	final ToggleButton togglePowerSafe = (ToggleButton) findViewById(R.id.TogglePowerSafe);
		if (power_safe == 1)
 		{
			togglePowerSafe.setChecked(true);
 			MotionActivity.power_safe = 1;
 			Gamepad.power_safe = 1;
 			Controller.power_safe = 1;
 		}
	 	else
	 	{
	 		togglePowerSafe.setChecked(false);
	 		MotionActivity.power_safe = 0;
	 		Gamepad.power_safe = 0;
 			Controller.power_safe = 0;
	 		
	 	}
	 	togglePowerSafe.setOnClickListener(new OnClickListener()
	 	{
	 		@Override
	 		public void onClick(View v) 
	 		{
	 			if(togglePowerSafe.isChecked())
	 			{
	 				Toast.makeText(getApplicationContext(), "Power safe mode is on", Toast.LENGTH_SHORT).show();
	 				SharedPreferences sharedPreferences = getSharedPreferences("MyOptions", Context.MODE_PRIVATE);
	 				SharedPreferences .Editor editor = sharedPreferences.edit();
	 				editor.putInt("power_safe", 1);
	 				editor.commit();
	 				MotionActivity.power_safe = 1;
	 		 		Gamepad.power_safe = 1;
	 	 			Controller.power_safe = 1;
	 			}
	 			else 
	 			{
	 				Toast.makeText(getApplicationContext(), "Power safe mode is off", Toast.LENGTH_SHORT).show();
	 				SharedPreferences sharedPreferences = getSharedPreferences("MyOptions", Context.MODE_PRIVATE);
	 				SharedPreferences .Editor editor = sharedPreferences.edit();
	 				editor.putInt("power_safe", 0);
	 				editor.commit();
	 				MotionActivity.power_safe = 0;
	 		 		Gamepad.power_safe = 0;
	 	 			Controller.power_safe = 0;
	 			}
	 		}
		});
	 	
	 	final ToggleButton toggleAnalogMode = (ToggleButton) findViewById(R.id.ToggleAnalogMode);
	 	if (gamepad_mode == 1)
 		{
	 		toggleAnalogMode.setChecked(true);
 			AnalogueView.gamepad_mode = 1;
 		}
	 	else
	 	{
	 		toggleAnalogMode.setChecked(false);
	 		AnalogueView.gamepad_mode = 0;
	 		
	 	}
	 	toggleAnalogMode.setOnClickListener(new OnClickListener()
	 	{
	 		@Override
	 		public void onClick(View v) 
	 		{
	 			if(toggleAnalogMode.isChecked())
	 			{
	 				Toast.makeText(getApplicationContext(), "Absolute-Relative analog mode is on", Toast.LENGTH_SHORT).show();
	 				SharedPreferences sharedPreferences = getSharedPreferences("MyOptions", Context.MODE_PRIVATE);
	 				SharedPreferences .Editor editor = sharedPreferences.edit();
	 				editor.putInt("gamepad_mode", 1);
	 				editor.commit();
	 				AnalogueView.gamepad_mode = 1;
	 			}
	 			else 
	 			{
	 				Toast.makeText(getApplicationContext(), "Normal analog mode is on", Toast.LENGTH_SHORT).show();
	 				SharedPreferences sharedPreferences = getSharedPreferences("MyOptions", Context.MODE_PRIVATE);
	 				SharedPreferences .Editor editor = sharedPreferences.edit();
	 				editor.putInt("gamepad_mode", 0);
	 				editor.commit();
	 				AnalogueView.gamepad_mode = 0;
	 			}
	 		}
		});
	 	
	
		
		
	 	
	 
	 	
	 	
	 	
	 	

	}

	

}

