package com.example.androidwifimouse;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;


public class Controller extends Activity implements OnTouchListener, OnKeyListener{
	
	int lastXpos = 0;
	int lastYpos = 0;
	boolean keyboard = false;
	Thread checking;
	public static final int DEFAULT_POWER = 0;
	
	public static int power_safe;
	
	
	String delim = new String("!!");
	
	protected void onCreate(Bundle savedInstanceState) {
		
	 	SharedPreferences sharedPreferences = getSharedPreferences ("MyOptions" , Context.MODE_PRIVATE);
        int power_safe = sharedPreferences.getInt("power_safe" , DEFAULT_POWER);
		
		
		if(power_safe == 0){
		//sprjecava gasenje ekrana ukoliko je neaktivan
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}
		
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.control);
		
	 	Display display = getWindowManager().getDefaultDisplay(); 
	 	@SuppressWarnings("deprecation")
		int width = display.getWidth();
	 	
	 	Button left = (Button) findViewById(R.id.LeftClickButton);
	 	Button right =  (Button) findViewById(R.id.RightClickButton);
	 	
		left.setWidth(width/2);
	 	right.setWidth(width/2);
	 	
	    View touchView = (View) findViewById(R.id.TouchPad);
	    touchView.setOnTouchListener(this);
	    
	    EditText editText = (EditText) findViewById(R.id.KeyBoard);
	    editText.setOnKeyListener(this);
	    editText.addTextChangedListener(new TextWatcher(){
		    public void  afterTextChanged (Editable s){
		    	Log.d("seachScreen", ""+s);
		    	s.clear();
	        } 
	        public void  beforeTextChanged  (CharSequence s, int start, int count, int after){ 
	                Log.d("seachScreen", "beforeTextChanged"); 
	        } 
	        public void  onTextChanged  (CharSequence s, int start, int before, int count) {
	        	Klijent appDel = ((Klijent)getApplicationContext());
	        	
	        	try{
	        		char c = s.charAt(start);
	        		appDel.sendMessage("KEY"+delim+c);
	        	}
	        	catch(Exception e){}
	        }
	    });
	}
	
	public void onStart(){
		super.onStart();

		Klijent appDel = ((Klijent)getApplicationContext());
		sendToAppDel(new String("Mouse Sensitivity!!"+appDel.mouse_sensitivity));
		
			new Thread(new Runnable(){
				Klijent appDel = ((Klijent)getApplicationContext());
				public void run(){
					while(appDel.connected){
						try{Thread.sleep(1000);}
						catch(Exception e){};
						if(!appDel.connected){
							finish();
						}
					}
				}
			}).start();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		mousePadHandler(event);
	 	return true;
	}
	
	@Override
	public boolean onKey(View v, int c, KeyEvent event){
		Log.d("ello", ""+event.getKeyCode());
		Klijent appDel = ((Klijent)getApplicationContext());
		
	 	appDel.sendMessage("S_KEY"+delim+event.getKeyCode());
		return false;
	}


	private void sendToAppDel(String message){
		Klijent appDel = ((Klijent)getApplicationContext());
		if(appDel.connected){
			appDel.sendMessage(message);
		}
		else{
			finish();
		}
	}

    private void mousePadHandler(MotionEvent event) {
 	   StringBuilder sb = new StringBuilder();
 	   
 	   int action = event.getAction();
 	   int touchCount = event.getPointerCount();
 	   
 	   if(touchCount == 1){
			switch(action){
				case 0: sb.append("DOWN"+delim);
						sb.append((int)event.getX()+delim);
						sb.append((int)event.getY()+delim);
						break;
				
				case 1: sb.append("UP"+delim);
						//sb.append(event.getDownTime()+delim);
						//sb.append(event.getEventTime());
						break;
				
				case 2: sb.append("MOVE"+delim);
						sb.append((int)event.getX()+delim);
						sb.append((int)event.getY());
						break;
						
				default: break;
			}
 	   }

 	   else if(touchCount == 2){
 		   sb.append("SCROLL"+delim);
 		   if(action == 2){
 			  sb.append("MOVE"+delim);
 			  sb.append((int)event.getX()+delim);
			  sb.append((int)event.getY());
 		   }
 		   else
 			   sb.append("DOWN");
 	   }
 	 
 	  
 	  sendToAppDel(sb.toString());
 	}
    
    public void LeftButtonClickHandler(View v){
    	Log.d("eloo", "CLICKED");
    	sendToAppDel("CLICK"+delim+"LEFT");
    }
    
    public void RightButtonClickHandler(View v){
    	sendToAppDel("CLICK"+delim+"RIGHT");
    }
    

    public void keyClickHandler(View v){
    	EditText editText = (EditText) findViewById(R.id.KeyBoard);
    	InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    	
    	if(keyboard){
    		mgr.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    		keyboard = false;
    	}
    	else{
    		mgr.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    		keyboard = true;
    	}
    		
    	Log.d("SET", "Foucs");
    }
}

