package com.example.androidwifimouse;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

public class AnalogueView extends View {

	// public variables
	public static SharedPreferences preferences;
	public static int gamepad_mode;
	public static int pozicija;
	public static int LONG_PRESS_TIME = 1500;
	

	
	public static int test_neki = 0;

	// other
	DisplayMetrics metrics = getResources().getDisplayMetrics();
	OnMoveListener moveListener;
	OnTouchListener touchListener;
	String delim = new String("!!");

	Paint black = new Paint();
	Paint grey = new Paint();
	Paint white = new Paint();

	/* long */
	long down;
	long up;
	long razlika;

	/* boolean */
	boolean trajanje = false;

	/* int */
	final int RADIUS = 80;
	//galaxy
	//final int RADIUS = 80;
	
	
	int prvi_put, drugi_put;
	int width = metrics.widthPixels;
	int height = metrics.heightPixels;
	int i = 0;
	int prvi_dodir = 0;
	int rub = 0;
	int moving = 0;
	int cx, cy, w, h;
	int resetic = 0;
	int pomak = 0;
	private int toDo;

	/* float */
	// accelariton for moving
	float acc = 2.3f;
	float x_pos, y_pos;
	float x_centar, y_centar, stari_dodir_x, stari_dodir_y;
	float x, y;
	float a, b;

	/* double */
	double r, t;
	double stari_t;

	public AnalogueView(Context context, AttributeSet attrs) {
		super(context, attrs);
		black.setColor(Color.BLACK);
		grey.setColor(Color.GRAY);
		white.setColor(Color.WHITE);
		black.setFlags(Paint.ANTI_ALIAS_FLAG);
		white.setFlags(Paint.ANTI_ALIAS_FLAG);
		grey.setFlags(Paint.ANTI_ALIAS_FLAG);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		this.w = w;
		this.h = h;
		cx = w / 2;
		cy = h / 2;
		x = cx;
		y = cy;
		super.onSizeChanged(w, h, oldw, oldh);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		drawMyStuff(canvas);
		switch (toDo) {
		case 1:
			center();
			break;
		default:
			break;
		}

	}

	/* draw gamepads function */
	// drawCircle (x_position, y_position, size , color)
	private void drawMyStuff(final Canvas canvas) {
		//canvas.drawCircle(cx, cy, w / 5, black);
		//canvas.drawCircle(cx, cy, w / 2 - 5, grey);
		//canvas.drawCircle(cx, cy, w / 2 - 10, black);
		//canvas.drawCircle(x, y, RADIUS * 1.5f, grey);
		
		
		canvas.drawCircle(cx, cy, w/2, black);
        canvas.drawCircle(cx, cy, w/2-5, grey);
        canvas.drawCircle(cx, cy, w/2-10, black);
        //galaxy
        canvas.drawCircle(x, y, RADIUS*1.5f, grey);
		
	}

	// n2p : normal to polar coordinates conversion
	// p2n : polar to normal coordinates conversion
	// R : distance to polar center
	// T : polar angle

	double n2pR(double x, double y) {

		return distance(x, y, cx, cy);
	}

	double n2pT(double x, double y) {

		return Math.atan2((y - cy), (x - cx));
	}

	double p2nX(double r, double t) {

		return r * Math.cos(t) + cx;

	}

	double p2nY(double r, double t) {

		return r * Math.sin(t) + cy;
	}

	double n2pR() {
		return distance(x, y, cx, cy);
	}

	double n2pT() {
		return Math.atan2((y - cy), (x - cx));
	}

	double p2nX() {

		return (r * Math.cos(t) + cx);

	}

	double p2nY() {

		return (r * Math.sin(t) + cy);
	}

	double p2nX_n(int x_pom) {
		return (r * Math.cos(t) + cx);

	}

	double p2nY_n(int y_pom) {
		return (r * Math.sin(t) + cy);
	}

	double distance(double x1, double y1, double x2, double y2) {
		return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		switch (event.getAction()) {

		case MotionEvent.ACTION_DOWN:

			
			
			// 1 - send DOWN command to server, 2 - MOVE command
			moveListener.onHalfMoveInDirection(1, t, (int) event.getX(),
					event.getY());
			
			
			
			break;

		case MotionEvent.ACTION_MOVE:

			
		
			
			updatePosition(event);

			if (gamepad_mode == 0) // normal mode active
			{
				moveListener.onHalfMoveInDirection(2, t, (int) event.getX(),
						event.getY());
			}

			if (gamepad_mode == 1) // a-r mode active
			{
				//ovo otk
				//moving = 1;
				//samsung galaxy s5
				// _handler.removeCallbacks(_longPressed, LONG_PRESS_TIME);
				_handler.postDelayed(_longPressed, LONG_PRESS_TIME);
			}

			break;

		case MotionEvent.ACTION_UP:

			toDo = 1;

			if (gamepad_mode == 0) // normal mode active
			{
				center();
			}

			if (gamepad_mode == 1) // a-r mode active
			{
				//vratit
				_handler.removeCallbacks(_longPressed);
			//	moving = 0;
			//	rub = 0;
			//	center();
				 
			}

			break;

		default:
			break;

		}

		return true;
	}

	private void center() {

		/* gamepad back to center */
		if (r > 15) {
			r -= 15;
		} else {
			toDo = 0;
			r = 0;
		}

		if (gamepad_mode == 0) {
			x = (float) p2nX();
			y = (float) p2nY();

		}
		if (gamepad_mode == 1) {
			x = cx;
			y = cy;
		}

		invalidate();

	}

	

	
	
	final Handler _handler = new Handler();
	
	Runnable _longPressed = new Runnable() {
		public void run() {

			
			/* if edge */
			if (rub == 1) {
				/* depends on postition angle */
				// move UP
				if (t >= -1.875 && t < -1.275) {

					y = (float) p2nY() - i;
					x = (float) p2nX();
					moveListener.onMaxMoveInDirection(1, t, x, y);
					i++;
					_handler.postDelayed(_longPressed, 1500);
				}
				// move LEFT-UP
				if (t >= -2.70 && t < -1.875) {

					y = (float) p2nY() - i;
					x = (float) p2nX() - i;
					moveListener.onMaxMoveInDirection(1, t, x, y);
					i++;
					_handler.postDelayed(_longPressed, 1500);
				}
				// move RIGHT-UP
				if (t >= -1.275 && t < -0.35) {

					y = (float) p2nY() - i;
					x = (float) p2nX() + i;
					moveListener.onMaxMoveInDirection(1, t, x, y);
					i++;
					_handler.postDelayed(_longPressed, 1500);
				}
				// move LEFT
				if (t > 2.70 || t < -2.70) {

					x = (float) p2nX() - i;
					y = (float) p2nY();
					moveListener.onMaxMoveInDirection(1, t, x, y);
					i++;
					_handler.postDelayed(_longPressed, 1500);

				}
				// move RIGHT
				if (t >= -0.35 && t < 0.35) {

					x = (float) p2nX() + i;
					y = (float) p2nY();
					moveListener.onMaxMoveInDirection(1, t, x, y);
					i++;
					_handler.postDelayed(_longPressed, 1500);

				}
				// move DOWN
				if (t >= 1.275 && t < 1.875) {

					y = (float) p2nY() + i;
					x = (float) p2nX();
					moveListener.onMaxMoveInDirection(1, t, x, y);
					i++;
					_handler.postDelayed(_longPressed, 1500);
				}
				// move RIGHT-DOWN
				if (t >= 0.35 && t < 1.275) {

					y = (float) p2nY() + i;
					x = (float) p2nX() + i;
					moveListener.onMaxMoveInDirection(1, t, x, y);
					i++;
					_handler.postDelayed(_longPressed, 1500);
				}

				// move LEFT-DOWN
				if (t > 1.875 && t <= 2.70) {

					y = (float) p2nY() + i;
					x = (float) p2nX() - i;
					moveListener.onMaxMoveInDirection(1, t, x, y);
					i++;
					_handler.postDelayed(_longPressed, 1500);
				}
			}
			/* no edge */
			else {

				i = 0;
			}
		}
	};

	/* update position of stick */
	void updatePosition(MotionEvent e) {

		if (gamepad_mode == 0) {

			r = Math.min(w / 2 - RADIUS, n2pR(e.getX(), e.getY()));
			t = n2pT(e.getX(), e.getY());
			x = (float) p2nX();
			y = (float) p2nY();
			invalidate();

		}
		if (gamepad_mode == 1) {
			
			//r = Math.min(w / 2 - RADIUS, n2pR(e.getX(), e.getY()));
			
			r = Math.min(w / 2 - RADIUS, n2pR(e.getX(), e.getY()));
			t = n2pT(e.getX(), e.getY());
			x = (float) p2nX();
			y = (float) p2nY();
			
			/* if moving */
			if (moveListener != null) {
				/* if edge */
				//if (r == w / 2 - RADIUS) 
				if (r == w / 2 - RADIUS) {
					rub = 1;
				}
				/* no edge, move cursor */
				else {
					rub = 0;
					moveListener.onHalfMoveInDirection(2, t, x, y);
					
				}

			}
			//otk ovo
			invalidate();
		}
	}

	public void setOnMoveListener(OnMoveListener listener) {
		moveListener = listener;
	}

	public interface OnMoveListener {
	
		public void onHalfMoveInDirection(int a, double polarAngle,
				float smjerx, float smjery);

		public void onMaxMoveInDirection(int b, double polarAngle,
				float smjerx, float smjery);
	}
}