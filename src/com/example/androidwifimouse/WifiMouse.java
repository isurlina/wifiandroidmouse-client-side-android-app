package com.example.androidwifimouse;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;


import android.widget.TextView;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class WifiMouse extends Activity{
	private EditText ipField;
	private EditText portField;
	private AlertDialog alert;
	private AlertDialog network_alert;
	private SeekBar sensitivity;
	private TextView sens_value;
	
	
	private boolean firstRun = true;
	 RadioButton radio;
     RadioButton radiob;  
     RadioButton radioc;  
   
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
	
		
       
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		radio=(RadioButton) findViewById(R.id.touch);
		radiob=(RadioButton) findViewById(R.id.motion);
		radioc=(RadioButton) findViewById(R.id.gamepad);
		
		
	
		

       //  addListenerOnButton();
		ipField = (EditText) findViewById(R.id.EditText01);
		portField = (EditText) findViewById(R.id.EditText02);
		sensitivity = (SeekBar) findViewById(R.id.SeekBar01);
		
		
		sens_value = (TextView) findViewById(R.id.textView1);
		
		
		
		

	    sensitivity.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {       
	        @Override       
	        public void onStopTrackingTouch(SeekBar seekBar) {            
	        }       
	        @Override       
	        public void onStartTrackingTouch(SeekBar seekBar) {     
	        }       
	        @Override       
	        public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
	        	 say_minutes_left(progress);
	        }
	    });
	  
	
		
		
		
		
		ipField.setText("192.168.1.2");	
		portField.setText("5444");
		
		alert = new AlertDialog.Builder(this).create();
	    alert.setTitle("Nedostupna veza sa serverom");
	    alert.setMessage("Provjerite je li server pokrenut na vasem racunalu");
	    alert.setButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				return;
			}
		});
	    
	    network_alert = new AlertDialog.Builder(this).create();
	    network_alert.setTitle("Mreza nedostupna");
	    network_alert.setMessage("Vas uredjaj nije spojen na mrezu.");
	    network_alert.setButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				return;
			}
		});
	}
	

	private void say_minutes_left(int how_many)
	{
	    String what_to_say = String.valueOf(how_many);
	    sens_value.setText(what_to_say);
	}
	
	  // Initiating Menu XML file (menu.xml)
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.layout.menu, menu);
        return true;
    }
	
    

  
    
    
	/* novo menu*/
	  @Override
	    public boolean onOptionsItemSelected(MenuItem item)
	    {
	         
	        switch (item.getItemId())
	        {
	    
	 
	        case R.id.menu_share:
	        	Intent myIntent1 = new Intent(WifiMouse.this, options.class);
	        	WifiMouse.this.startActivity(myIntent1);
	        	//Toast.makeText(WifiMouse.this, "Share is Selected", Toast.LENGTH_SHORT).show();
	            return true;
	 
	        case R.id.menu_about:
	        	Intent myIntent = new Intent(WifiMouse.this, about.class);
	        	WifiMouse.this.startActivity(myIntent);
	            Toast.makeText(WifiMouse.this, "About application", Toast.LENGTH_SHORT).show();
	            return true;
	 
	        case R.id.menu_exit:
	            Toast.makeText(WifiMouse.this, "Closing application.", Toast.LENGTH_SHORT).show();
	            System.exit(0);
	            return true;
	 
	        default:
	            return super.onOptionsItemSelected(item);
	        }
	    }    
	
	
	@Override
	public void onResume(){
		Log.d("RESUME", "RESUMED");
		super.onResume();
		Klijent appDel = ((Klijent)getApplicationContext());
		
		if(!appDel.connected && !firstRun){
			alert.show();
		}
		
		appDel.stopServer();
	}
	
	@Override
	public void onPause(){
		super.onPause();
		firstRun = false;
	}
	
	
	

	
	
	public void clickHandler(View view) {
		Klijent appDel = ((Klijent)getApplicationContext());
		int s = sensitivity.getProgress();
		appDel.mouse_sensitivity = Math.round(s/20) + 1;
		
		
		
		if(!appDel.connected){
			String serverIp;
			int serverPort;
			
			serverIp = ipField.getText().toString();
			serverPort = Integer.parseInt(portField.getText().toString());
			
			appDel.createClientThread(serverIp, serverPort);
		}
		
		int x;
		for(x=0;x<4;x++){
			if(appDel.connected){
			//	int selectedId = radio.getCheckedRadioButtonId();
				
				if (radio.isChecked())
				{
				startActivity(new Intent(view.getContext(), Controller.class));
				}
				if (radiob.isChecked())
				{
					startActivity(new Intent(view.getContext(), MotionActivity.class));
				}
				if (radioc.isChecked())
				{
					startActivity(new Intent(view.getContext(), Gamepad.class));
				}
				x = 6;
			}
			try{Thread.sleep(250);}
			catch(Exception e){}
		}
		
		if(!appDel.connected)
			if(!appDel.network_reachable)
				network_alert.show();
			else
				alert.show();
	}
	

    
}

